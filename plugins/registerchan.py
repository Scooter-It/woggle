"""
Woggle - bot framework

  Copyright (C) 2024 default <coder@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from helpers import auth, sa
from sopel import plugin
import time

class RegisterChannels:
    """
    This is the class to register channels. This is one of the JotiGeneral
    plugins to only be used during joti.

    Config:

    [JotiGeneral]
    ops_channel = #beneluxops
    bot_channel = #dutch-bots
    coord = max
    cos = Thijseigenwijs, Vaandrig
    ops = Suikerklontje, zeilertje
    sups = None
    chanserv = chanserv
    operserv = operserv
    corights = "+votsrihqaRAF"
    oprights = "+varioe"
    suprights = "+Vvi"
    channels = #dutch01, #dutch02, #dutch03

    [RegisterChan]
    default_topic = "ScoutLink #dutch JOTI Channel (CHANNEL CLOSED)"
    chanmode = "+dsplitn 1 override"

    [permissions]
    registerchan = thijseigenwijs, Vaandrig

    """
    
    def __init__(self, bot):
        self.botchan = bot.config.JotiGeneral.bot_channel
        self.botchantt = bot.config.JotiGeneral.bot_channel
        self.chanserv = bot.config.JotiGeneral.chanserv
        self.channels = self.channels = bot.config.JotiGeneral.channels.translate(
            str.maketrans("", "", " \n\t\r")
        ).split(",")
        
        print(self.channels)
        print("[RegisterChannels] Setup complete")
        return
    
    
    def register_channel(self, bot, channel):
        # Create the channel
        bot.say(f"Joining {channel}", self.botchan)
        bot.join(channel)
        return
    
    
    def deregister_channel(self, bot, channel):
        return
    
    
    def cmd_single(self, bot, trigger, register=True, override=False):
        return
    
    
    def cmd_multi(self, bot, trigger, register=True, override=False):
        return
    
    def cmd_all(self, bot, trigger, register=True, override=False):
        for channel in self.channels:
            if(register):
                self.register_channel(bot, channel)
            else:
                self.deregister_channel(bot, channel)
        return
    
    
    
REGISTER = None

def setup(bot):
    global REGISTER
    REGISTER = RegisterChannels(bot)
    


@plugin.commands("register-all")
def register_all(bot, trigger):
    global REGISTER
    REGISTER.cmd_all(bot, trigger, register=True)