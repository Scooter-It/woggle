"""
Woggle - bot framework

  Copyright (C) 2021 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>
  Copyright (C) 2021 Liam ScoutLink <liam1@scoutlink.net>
  Copyright (C) 2021 Yorick Bosman <yorick@gewoonyorick.nl>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import time

import sopel.plugin as plugin

from helpers import auth, timer
from helpers.channelmanagement import blocknickchanges, moderated, voice
from helpers.sa import sajoin, sanick, sapart


class MurderMystery:
    """
    Config:

    [murdermystery]
    channel_game = #murder-mystery
    channel_management = #murder-mystery-control
    channel_death = #murder-mystery-deathchat
    deathchat_enabled = True

    [permissions]
    murdermystery = "admin1, admin2"
    """

    def __init__(self, bot):
        self.players = {}
        self.roles = {}
        self.game_status = "SETUP"
        self.game_channel = bot.config.murdermystery.channel_game
        self.management_channel = bot.config.murdermystery.channel_management
        self.deathchat_channel = bot.config.murdermystery.channel_death
        self.deathchat_enabled = bot.config.murdermystery.deathchat_enabled
        self.messages = {
            "intro": [],
            "epilogue": [],
            "role_prefix": [],
            "role_suffix": [],
        }

    def status_get(self, prettified=False):
        """
        Returns the game status (and optionally prettified)
        """
        status = self.game_status

        if prettified is False:
            return status

        prettify_map = {
            "SETUP": "\x0308Setting up\x0F",
            "PENDING": "\x0307Pending\x0F",
            "STARTED": "\x0303Started\x0F",
            "STOPPED": "\x0304Stopped\x0F",
        }

        return prettify_map[status]

    def player_get_role(self, player):
        """
        Returns the player's role or False if the player doesn't exist
        """
        if player in self.players:
            return self.players[player]["role"]
        return False

    def player_get_nick(self, role):
        """
        Returns a player entry based on the provided role
        """
        for user in self.players:
            if self.players[user]["role"] == role:
                return user
        return None

    def player_get_role_nick(self, player):
        """
        Returns a dict of of the player and its role.
        """
        if player in self.players:
            role = self.player_get_role(player)
            return {"nick": player, "role": role}

    def get_text_arg(self, args, role_remove=True):
        """
        Parses an incoming command trigger
        """
        cmd = args.group(3)
        role = args.group(4)
        text = args.group(2)
        if role_remove:
            return text.replace(f"{cmd} {role} ", "", 1)
        return text.replace(f"{cmd} ", "", 1)

    #
    #   Intro & Epilogue messages
    #
    #   Intro mesages get displayed at the start of the game, epilogue when the game ends.
    #

    def text_send(self, bot, kind, to=None):  # pylint: disable=invalid-name
        """
        Send intro, epilogue, role_prefix or role_suffix to the game channel or otherwise supplied
        receiver
        """
        if to is None:
            to = self.game_channel
        for message in self.messages[kind]:
            bot.say(message, to)

    # Kill timer

    @timer.resetable_timer(600)
    def kill_timer(self, bot):
        """
        A timer to remind the management channel when it is time to kill someone.
        """
        bot.say(
            "It has been 10 minutes since the last kill. It is suggested to kill someone again "
            "now.",
            self.management_channel,
        )

    # Private messages

    def pm_incoming(self, bot, trigger):
        """
        Handles incoming pm messages, private messages will be send to the game management channel
        Private messages will only go through if the game has been started and if they are a player.
        Otherwise a message will be send back to the user to inform them.
        """
        message_template = "Sorry, your private message has been discarded as "
        message_reasons = {
            "NOT_STARTED": "there is no ongoing murdermystery game.",
            "NOT_PLAYER": "you are not a player in the murdermystery game.",
        }

        if trigger[0] != bot.config.core.help_prefix:
            if self.game_status == "STARTED":
                if trigger.nick in self.roles:
                    realnick = self.player_get_nick(trigger.nick)
                    if realnick is None:
                        bot.say(
                            f"Warning, {trigger.nick} hasn't been properly assigned to their role. "
                            + "Please assign them to their correct role.",
                            self.management_channel,
                        )
                    return bot.say(
                        f"PM | {trigger.nick}{' (' + realnick + ')' if realnick else ''}: "
                        + trigger.group(0),
                        self.management_channel,
                    )

                return bot.say(
                    message_template + message_reasons["NOT_PLAYER"],
                    trigger.nick,
                )

            bot.say(
                message_template + message_reasons["NOT_STARTED"],
                trigger.nick,
            )

    # Commands

    def command_pm(self, bot, trigger):
        """
        Allows sending private messages to players.
        """
        receiver = None
        sender = ""
        message_template = "Sorry, your private message has been discarded as "
        message_reasons = {
            "NOT_STARTED": "there is no ongoing murdermystery game, start it first.",
            "NOT_PLAYER": "that user is not part of a murdermystery game.",
        }

        # Check if the game has been started
        if self.game_status != "STARTED":
            return bot.say(
                message_template + message_reasons["NOT_STARTED"],
                trigger.sender,
            )

        # Check if the user is a player
        if trigger.group(4) in self.roles:
            receiver = trigger.group(4)
        elif self.player_get_role(trigger.group(4)) in self.roles:
            receiver = self.player_get_role(trigger.group(4))

        else:
            return bot.say(
                message_template + message_reasons["NOT_PLAYER"],
                trigger.sender,
            )

        # Check if the sender is a player
        if (
            trigger.nick in self.players
            or self.player_get_nick(trigger.nick) in self.players
        ):
            sender = f" ({self.player_get_nick(trigger.nick)})"

        # Get message
        message = trigger.split(" ", maxsplit=3)[3]

        # Send the pm
        bot.say(f"{trigger.nick}{sender}: {message}", receiver)

    def command_text_add(self, bot, trigger, kind):
        """
        Handles the text add command which adds messages for a kind (intro, epilogues etc)
        """
        text = self.get_text_arg(trigger, role_remove=False)
        self.messages[kind].append(text)
        bot.say(
            f"The text has been added succesfully to the {kind}, to view all messages use the "
            f".mm show{kind} command."
        )

    def command_text_clear(self, bot, trigger, kind):  # pylint: disable=unused-argument
        """
        Handles the text clear command which clears messages for a kind (intro, epilogues etc)
        """
        self.messages[kind] = []
        bot.say(f"Succesfully cleared the {kind} messages!")

    def command_text_show(self, bot, trigger, kind):  # pylint: disable=unused-argument
        """
        Handles the text show command which shows messages for a kind (intro, epilogues etc)
        """
        messages = self.messages[kind]
        if messages == []:
            bot.say(f"No messages are configured for {kind}!")
        else:
            num = len(messages)
            bot.say(f"{num} messages are configured for the {kind}:")
            for message in messages:
                bot.say(message)
            bot.say(f"End of messages for the {kind}!")

    def command_add_role(self, bot, trigger):
        """
        Handles the add role command which adds a new role
        """
        role = trigger.group(4).lower()
        if role in self.roles:
            bot.say(f"The role {role} already excists!")
        else:
            self.roles[role] = {"start_messages": []}
            bot.say(f"The role {role} has been created succesfully!")

    def command_remove_role(self, bot, trigger):
        """
        Handles the remove role command which removes the specified role
        """
        role = trigger.group(4).lower()
        if role in self.roles:
            self.roles.pop(role)
            bot.say(f"The role {role} has been removed succesfully!")
        else:
            bot.say(f"The role {role} doesn't excist!")

    def command_add_messages(self, bot, trigger):
        """
        Handles the add messages command which add a message to a role
        """
        text = self.get_text_arg(trigger)
        role = trigger.group(4).lower()
        if role in self.roles:
            self.roles[role]["start_messages"].append(text)
            bot.say(
                f"The text has been added succesfully to the role {role}, to view all messages use"
                " the .mm showmessages <role> command."
            )
        else:
            bot.say(f"The role {role} doesn't excist!")

    def command_show_messages(self, bot, trigger):
        """
        Handles the show messages command which shoes the messages for a role
        """
        role = trigger.group(4).lower()
        if role in self.roles:
            messages = self.roles[role]["start_messages"]
            if messages == []:
                bot.say(f"No messages are configured for the role {role}!")
            else:
                num = len(messages)
                bot.say(f"{num} messages are configured for the role {role}:")
                for message in messages:
                    bot.say(message)
                bot.say(f"End of messages for role {role}!")
        else:
            bot.say(f"The role {role} doesn't excist!")

    def command_clear_messages(self, bot, trigger):
        """
        Handles the clear messages command which removes all messages from a role
        """
        role = trigger.group(4).lower()
        if role in self.roles:
            self.roles[role]["start_messages"] = []
            bot.say(f"Succesfully cleared messages for the role {role}!")
        else:
            bot.say(f"The role {role} does not excist!")

    def command_assign_role(self, bot, trigger):
        """
        Handles the assign role command which assign a role to a player
        """
        player = trigger.group(4)
        role = trigger.group(5).lower()

        # Check if player has a role
        if player in self.players:
            player_role = self.player_get_role(player)
            bot.say(
                f"{player} already has the role {player_role}! Remove this role before assigning"
                " a new one."
            )

        else:
            # Check if role excists
            if role in self.roles:
                self.players[player] = {"role": role, "nickname": player, "dead": False}
                bot.say(f"The role {role} has succesfully been assigned to {player}")
                if self.game_status == "STARTED":
                    # Do sanicks & send messages
                    # Send role prefix
                    self.text_send(bot, "role_prefix", to=player)

                    messages = self.roles[role]["start_messages"]
                    for message in messages:
                        bot.say(message, player)

                    # Send role suffix
                    self.text_send(bot, "role_prefix", to=player)

                    voice(bot, player, self.game_channel)

                    # wait for the stuff to get send
                    time.sleep(1)
                    sanick(bot, player, role)
            else:
                bot.say(f"The role {role} does not excist!")

    def command_kill_player(self, bot, trigger):
        """
        Handles the kill player command which kills a player
        """
        target = trigger.group(4)
        playername = None

        if target in self.players:
            playername = target
        if target in self.roles:
            playername = self.player_get_nick(target)

        player_data = self.player_get_role_nick(playername)
        if player_data is not None:
            if self.players[player_data["nick"]]["dead"]:
                return bot.say(
                    "You really want to be sure that they're dead? (Already dead)"
                )
            else:
                self.players[player_data["nick"]]["dead"] = True
                voice(bot, player_data["role"], self.game_channel, False)

                # Reset kill game timer
                self.kill_timer(bot).reset(start=True)
                bot.say(
                    f"Player {player_data['role']} has been killed!",
                    self.management_channel,
                )
                if self.deathchat_enabled:
                    sajoin(bot, player_data["role"], self.deathchat_channel)
                time.sleep(1)
                sanick(bot, player_data["role"], player_data["role"] + "_dead")
        else:
            bot.say(f"{playername} could not be found. Please try again.")

    def command_revoke_role(self, bot, trigger):
        """
        Handles the revoke role command which removes a player from a role
        """
        player = trigger.group(4)

        # Check if player has a role
        if player in self.players:
            role = self.player_get_role(player)
            self.players.pop(player)
            bot.say(
                f"The player {player} has succesfully been removed from the role {role}"
            )
        else:
            bot.say(f"{player} doesn't have a role yet.")

    def command_start_game(self, bot, trigger):  #  pylint: disable=unused-argument
        """
        Handles the start game which initializes the game.
        """
        currentstatus = self.status_get()
        if currentstatus != "STARTED":
            # Add check if all assigned role people are actually there

            # Do sanicks & send messages
            players = self.players
            for playerid in players:
                player = self.players[playerid]

                # Send role prefix
                self.text_send(bot, "role_prefix", to=player["nickname"])

                messages = self.roles[player["role"]]["start_messages"]
                for message in messages:
                    bot.say(message, player["nickname"])

                # Send role suffix
                self.text_send(bot, "role_prefix", to=player["nickname"])

                voice(bot, player["nickname"], self.game_channel)

            # wait for the stuff to get send
            time.sleep(1)
            for playerid in players:
                player = self.players[playerid]
                sanick(bot, player["nickname"], player["role"])

            # Set modes
            moderated(bot, self.game_channel)
            blocknickchanges(bot, self.game_channel)

            # Send intro
            self.text_send(bot, "intro")

            # Start game timer
            self.kill_timer(bot).start()

            # Change status
            self.game_status = "STARTED"
            bot.say(
                "The game has succesfully been started! See your pm for your role explanation!",
                self.game_channel,
            )
        else:
            bot.say("The game has already started.")

    def command_stop_game(self, bot, trigger):  # pylint: disable=unused-argument
        """
        Handles the stop game command which stops the games and resets nicknames and removes the
        users from the channels
        """
        currentstatus = self.status_get()
        if currentstatus == "STARTED":
            # Add check if all assigned role people are actually there

            # Do sanicks
            players = self.players
            for playerid in players:
                player = self.players[playerid]
                is_dead = self.players[player["nickname"]]["dead"]
                currentnick = player["role"]
                if is_dead:
                    currentnick = currentnick + "_dead"
                voice(bot, currentnick, self.game_channel, False)
                if self.deathchat_enabled:
                    if self.players[player["nickname"]]["dead"]:
                        sapart(
                            bot,
                            currentnick,
                            self.deathchat_channel,
                            "Murdermystery game ended.",
                        )

            # wait for the stuff to get send
            time.sleep(1)
            for playerid in players:
                player = self.players[playerid]
                currentnick = player["role"]
                is_dead = self.players[player["nickname"]]["dead"]
                if is_dead:
                    currentnick = currentnick + "_dead"
                sanick(bot, currentnick, player["nickname"])

            # Set modes
            moderated(bot, self.game_channel, False)
            blocknickchanges(bot, self.game_channel, False)

            # Send epilogue
            self.text_send(bot, "epilogue")

            # Stop game timer
            self.kill_timer(bot).cancel()

            # Change status
            self.game_status = "STOPPED"
            bot.say("The game has succesfully been stopped!", self.game_channel)
        else:
            bot.say("The game has to be started in order for it to stop")

    def command_clear_game(self, bot, trigger):  # pylint: disable=unused-argument
        """
        Handles the clear game command which removes all the users and roles
        """
        self.players = {}
        self.roles = {}
        self.game_status = "SETUP"
        bot.say("Cleared users & roles")

    def command_clear_users(self, bot, trigger):  # pylint: disable=unused-argument
        """
        Handles the clear users command which removes all the users
        """
        self.players = {}
        bot.say("Cleared users")

    def command_status(self, bot, trigger):  # pylint: disable=unused-argument
        """
        Handles the status command return the status of the game
        """
        status = self.status_get(prettified=True)
        bot.say(f"Game status is: {status}")
        status = self.status_get()
        if status == "SETUP":
            return  # TODO: implement status overview pre-game
        if status == "STARTED":
            return  # TODO: implement status overview when the game has started
        if status == "STOPPED":
            alive = []
            dead = []
            for playerid in self.players:
                player = self.players[playerid]
                if player["dead"]:
                    dead.append(player["nickname"])
                else:
                    alive.append(player["nickname"])
            alive_players = fancy_list_maker(alive)
            dead_players = fancy_list_maker(dead)
            bot.say(f"Players still alive: {alive_players}")
            bot.say(f"Players who died: {dead_players}")
        bot.say("End of status")

    def command_info(self, bot, trigger):
        """
        Handles the info command which returns the role of a user
        """
        player_req = trigger.group(4)
        player = self.player_get_role_nick(player_req)
        if player is None:
            bot.say("Player or role not found. Please try again.")
        else:
            role = player["role"]
            nick = player["nick"]
            bot.say(
                f"Player has the role \x02{role}\x0F and has the nickname \x02{nick}\x0F"
            )


def setup(bot):
    """
    Initializes the module
    """
    bot.memory["murder_mystery"] = MurderMystery(bot)


@plugin.commands("mm", "murdermystery")
@auth.require_permission("murdermystery")
def command(bot, trigger):
    """
    Handles all murdery mystery commands
    """
    if trigger.group(3) is None:
        return bot.say(
            "Please provide a subcommand. See the documentation for more information."
        )

    murder_mystery = bot.memory["murder_mystery"]
    command_map = {
        "createrole": murder_mystery.command_add_role,
        "removerole": murder_mystery.command_remove_role,
        "addmessage": murder_mystery.command_add_messages,
        "clearmessages": murder_mystery.command_clear_messages,
        "showmessages": murder_mystery.command_show_messages,
        "assign": murder_mystery.command_assign_role,
        "resign": murder_mystery.command_revoke_role,
        "kill": murder_mystery.command_kill_player,
        "start": murder_mystery.command_start_game,
        "stop": murder_mystery.command_stop_game,
        "cleargame": murder_mystery.command_clear_game,
        "clearusers": murder_mystery.command_clear_users,
        "status": murder_mystery.command_status,
        "info": murder_mystery.command_info,
        "pm": murder_mystery.command_pm,
        "showintro": [murder_mystery.command_text_show, "intro"],
        "showepilogue": [murder_mystery.command_text_show, "epilogue"],
        "showroleprefix": [murder_mystery.command_text_show, "role_prefix"],
        "showrolesuffix": [murder_mystery.command_text_show, "role_suffix"],
        "addintro": [murder_mystery.command_text_add, "intro"],
        "addepilogue": [murder_mystery.command_text_add, "epilogue"],
        "addroleprefix": [murder_mystery.command_text_add, "role_prefix"],
        "addrolesuffix": [murder_mystery.command_text_add, "role_suffix"],
        "clearintro": [murder_mystery.command_text_clear, "intro"],
        "clearepilogue": [murder_mystery.command_text_clear, "epilogue"],
        "clearroleprefix": [murder_mystery.command_text_clear, "role_prefix"],
        "clearrolesuffix": [murder_mystery.command_text_clear, "role_suffix"],
    }

    try:
        if isinstance(command_map[trigger.group(3)], list):
            command_map[trigger.group(3)][0](
                bot, trigger, command_map[trigger.group(3)][1]
            )
        else:
            command_map[trigger.group(3)](bot, trigger)
    except KeyError:
        bot.say(
            "Sorry, I didn't quite catch that. Please check the documentation to make sure the "
            "command you want to use is available."
        )


def fancy_list_maker(names):
    """
    Transforms a list to a human friendly string
    """
    string = ", ".join(names)
    return " and ".join(string.rsplit(", ", 1))


@plugin.rule(".*")
@plugin.require_privmsg
def pmlistener(bot, trigger):
    """
    Listen to all pms and forward them to the Murdermystery object to further deal with it.
    """
    bot.memory["murder_mystery"].pm_incoming(bot, trigger)
