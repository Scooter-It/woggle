Some kind of vegetable
A piece of fruit
A Jungle Book name
A sport
A board game
A movie
An actor
An actrice
A singer
A female singer
A television program
A cartoon figure
A character from a movie
A place of residence
A head of state
A presenter
A flower
An animal
A pet
An activity
A piece of furniture
A means of transport
A country
A province
A song
A Tree or Plant
Food and drink
A job
A Button
An abbreviation
A piece of clothing
A soccer club
A chair
A car brand
A farm animal
A family game
A maiden name
A boy's name
A country in Africa
An insect
Something yellow
Something blue
A family member
A movie
What do you take with you on vacation
A newspaper or magazine
A holiday
What can you drink
what can you collect
A TV series
Where can you live in
Something heavier than an elephant
something square
Something that can fly
Something red
Something that can swim
something green
Something that smells
What are you putting on bread
A name of an amusement park
Something you need in the summer
Something made of wood
Something you can sit on
Something made of metal
A title of a CD
A pet
An animal smaller than a cat
Something sweet
something salty
Something that makes noise
Something invisible
A colour
What gives light
Something you can make yourself
Something that has wings
A hero or villain
Some kind of vegetable
A fish
A politician
A school subject
Some kind of building material
A computer brand
A traffic sign
A snack
An education
A means of payment
A cheese brand
A name of a supermarket
A street name in Waalwijk
A car brand
A minister from the current cabinet
A political party
A kind of storage media
A child's play
A family game
A cell phone provider
An electricity company
A boat
A tent
A newspaper
A lamp brand
A weather condition
A country in the Alps
A country in Australia
A country in South America
Some kind of ground
A computer program
Some kind of kitchen utensils
A sanitary brand
A train station
A taxi company
A shoe brand
A coat rack
A name of a mountain
A bird
A program for the scouts
A program for the gnomes
A program for the cubs
A program for the explorers
A coffee brand
A university
A college
A provincial town
A capital
A highway
A ball game
A bathing place
A zoo
A Wadden Island
An object from a classroom
A room in a castle
Some kind of glasses
Part of a bicycle
Part of a car
Something you don't want to get in your nose
Part of the human body
An IRC operator
An action
Something from the supermarket
Something you shouldn't drop on your foot
Something you regret
A bank
Something you cannot make yourself
Something on wheels
Your favorite world dish
I'm going to camp and I'm taking with me
An app from the app store
Something you can do with your smartphone
Something you CANNOT do with your smartphone
A telephone brand
A social media platform
A Pokemon character
A youtuber with the letter
A YouTube channel
An alternative to Google
A Google service
An online Game
Something quietly from the years
A hype
A Disney movie
A Netflix series
Your favorite tea flavor
Your worst nightmare
Something you are a fan of
An emoticon
A dream
Something you can eat with chopsticks
Something you cannot see
Something otherworldly
A programming language
A drawing program
Something in your computer
A website
Your favorite web store
A Wikipedia topic
Something you do in the winter
Something you do during the JOTA-JOTI
Something you do at camp
Something that stinks
Something magnetic
Something typically Dutch
Something quintessentially American
A fairytale
Something you can find at the clubhouse
Something typical of Scouting
A campfire song
A camp theme
Something in the first aid kit
Something in a camp chest
Something you don't want to find in your shoe
A name of a Scouting group
The nicest Nickname on Scoutlink
The best pun
An IKEA product
Something you shouldn't sit on