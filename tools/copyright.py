#!/usr/bin/env python3
"""
Woggle - bot framework

  Copyright (C) 2021 Yorick Bosman <yorick@gewoonyorick.nl>
  Copyright (C) 2021,2022 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
import re
import subprocess
import sys
from datetime import datetime

DATE_FORMAT = "%a %b %d %H:%M:%S %Y %z"

# Due to file moving (i.e. from modules to plugins), we need to keep track of static records
with open("tools/copyrights.json") as json_file:
    STATIC_COPYRIGHTS = json.load(json_file)

COPYRIGHT_MESSAGE = (
    lambda: f"""
Woggle - bot framework

{AUTHORS_MSG}
This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
)

for my_file in sys.argv[1:]:

    # Check if the dir is a virtualenv, and skip checking if it is
    # We assume the virtualenv is stored in woggle or virtualenv
    directory = my_file.split("/")
    if directory[1] == "virtualenv" or directory[1] == "woggle":
        continue

    authors = STATIC_COPYRIGHTS.get(my_file) or {}
    try:
        result = subprocess.run(
            ["git", "log", "--use-mailmap", my_file],
            stdout=subprocess.PIPE,
            check=True,
        )
    except subprocess.CalledProcessError:
        print("The git command failed.")
        sys.exit(1)
    git_log_stdout = result.stdout.decode("utf8")
    for commit in git_log_stdout.split("commit"):
        try:
            author = re.search("Author: (.*)\n", commit)[1].strip()
            year = datetime.strptime(
                re.search("Date: (.*)\n", commit)[1].strip(), DATE_FORMAT
            ).year
            try:
                authors[author].add(year)
            except KeyError:
                authors[author] = set([year])
        except TypeError:
            pass

    AUTHORS_MSG = ""
    for author in authors:
        YEARS = ",".join([str(year) for year in authors[author]])
        AUTHORS_MSG += f"  Copyright (C) {YEARS} {author}\n"
    AUTHORS_MSG = "\n".join(sorted(AUTHORS_MSG.split("\n")[:-1])) + "\n"
    copyright_message = COPYRIGHT_MESSAGE()

    START_OF_FILE = True
    UPDATED_CONTENTS = ""
    IN_DOCSTRING = False
    DONE_DOCSTRING = False
    with open(my_file) as f:
        for line in f:
            if not (DONE_DOCSTRING or IN_DOCSTRING) and (
                line.startswith('"""') or line.startswith("'''")
            ):
                IN_DOCSTRING = True
                continue

            if IN_DOCSTRING and (
                line.strip().endswith('"""') or line.strip().endswith("'''")
            ):
                IN_DOCSTRING = False
                continue

            if START_OF_FILE:
                if line.startswith("#!"):
                    UPDATED_CONTENTS = line
                UPDATED_CONTENTS += '"""'
                UPDATED_CONTENTS += copyright_message
                UPDATED_CONTENTS += '"""\n'
                START_OF_FILE = False
                continue

            if not IN_DOCSTRING:
                UPDATED_CONTENTS += line
                DONE_DOCSTRING = True

    with open(my_file, "w") as f:
        f.write(UPDATED_CONTENTS)
